const noble = require('@abandonware/noble')
const Influx = require('influx')
const os = require('os')

const influx = new Influx.InfluxDB('http://10.0.1.200:8086/telegraf')

noble.on('stateChange', function (state) {
  if (state === 'poweredOn') {
    noble.startScanning([], true)
  } else {
    noble.stopScanning()
  }
})

noble.on('discover', function (peripheral) {
  const { advertisement } = peripheral
  const { localName, serviceData, manufacturerData } = advertisement
  var points = []
  if (manufacturerData && manufacturerData[0] == 0x33 && manufacturerData[1] == 0x01) {
    points = blueMaestroMetrics(localName, manufacturerData)
  }
  if (manufacturerData && manufacturerData[0] === 0xd2 && manufacturerData[1] === 0x00) {
    points = aSensorMetrics(localName, manufacturerData)
  }
  if (points && points.length > 0) {
    if (process.env.DEBUG) {
      points.map(console.log)
    } else {
      return influx.writePoints(points)
    }
  }
})


function aSensorMetrics (localName, manufacturerData) {
  const sensor = {
    name: localName,
    temp_c: manufacturerData[9],
    temp_f: manufacturerData[9] * 9 / 5 + 32,
    inMotion: (manufacturerData[10] === 1),
    x: manufacturerData[11],
    y: manufacturerData[12],
    z: manufacturerData[13],
    currentDutaion: manufacturerData[15],
    previousDuration: manufacturerData[15],
    battery: manufacturerData[16]
  }
  return [{
    measurement: 'temperature',
    tags: { host: os.hostname(), name: localName },
    fields: { fahrenheit: sensor.temp_f, celsius: sensor.temp_c }
  }, {
    measurement: 'motion',
    tags: { host: os.hostname(), name: localName },
    fields: { inMotion: sensor.inMotion, x: sensor.x, y: sensor.y, z: sensor.z, duration: sensor.currentDutaion }
  }, {
    measurement: 'battery',
    tags: { host: os.hostname(), name: localName },
    fields: { battery: sensor.battery }
  }]
}

function puckMetrics (localName, serviceData) {
  if (serviceData.length > 0) {
    var celsius, fahrenheit, light
    serviceData.forEach(function (service) {
      const { uuid, data } = service
      switch (uuid) {
        case '1809':
          celsius = data.readUInt8(0)
          fahrenheit = celsius * 9 / 5 + 32
          break
        case '181a':
          light = data.readUInt8(0)
          break
      }
    })

    return [{
      measurement: 'temperature',
      tags: { host: os.hostname(), name: localName },
      fields: { fahrenheit, celsius }
    }, {
      measurement: 'light',
      tags: { host: os.hostname(), name: localName },
      fields: { value: light }
    }]
  }
  return []
}

function blueMaestroMetrics (localName, manufacturerData) {
  // 33 01 0d 64 0e 10 00 37 00 d6 00 00 00 00 01 00
  // I see other advertisements with the same prefix (0x33 0x01) x
  // but the rest of the format doesn't match. So I use version to ignore them
  const version = manufacturerData[2]
  if (version === 13) { // basic temp sensor, the one I have)
    const battery = manufacturerData[3]
    const interval = manufacturerData.readUInt16BE(4)
    const counter = manufacturerData.readUInt16BE(6)
    const temperature = manufacturerData.readUInt16BE(8) / 10.0
    const humidity = manufacturerData.readUInt16BE(10)
    const mode = manufacturerData[14]
    var fahrenheit, celsius
    if (mode > 100) {
      fahrenheit = temperature
      celsius = (fahrenheit - 32) * 5 / 9
    } else {
      celsius = temperature
      fahrenheit = celsius * 9 / 5 + 32
    }
    return [{
      tags: { host: os.hostname(), name: localName },
      measurement: 'temperature',
      fields: { fahrenheit, celsius }
    }, {
      tags: { host: os.hostname(), name: localName },
      measurement: 'battery',
      fields: { battery }
    }]
  }
  return []
}

